#ifndef __TERM__HH__
#define __TERM__HH__

#define _XOPEN_SOURCE 600
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <iostream>

#define SHELL "/bin/bash"

#include "pty.hh"

class Terminal
{
public:
  int fd, screen, w, h, font_width, font_height, buf_w, buf_h, buf_y, buf_x;
  
  Display* disp;
  Window root;
  Window termwin;
  GC gc;
  XFontStruct* xfont;
  
  unsigned long col_fg, col_bg;
  char* buf;
};

void Redraw(Terminal*);
bool Setup(Terminal*);

#endif
