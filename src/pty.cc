#include "pty.hh"

PTY::PTY()
{
}

PTY::~PTY()
{
}


bool pt_pair(PTY* pty)
{
  char* SlaveName;
  pty->master = posix_openpt(O_RDWR | O_NOCTTY);
  if (pty->master == -1)
    {
      std::cerr << "posix_openpt failed" << std::endl;
      return false;
    }
  if (grantpt(pty->master) == -1)
    {
      std::cerr <<  "grantpt failed" << std::endl;
      return false;
    }
  
  if (unlockpt(pty->master) == -1)
    {
      std::cerr << "unlockpt failed" << std::endl;
      return false;
    }
  
  SlaveName = ptsname(pty->master);
  if (SlaveName == NULL)
    {
      std::cerr << "ptsname failed" << std::endl;
      return false;
    }
  
  pty->slave = open(SlaveName, O_RDWR | O_NOCTTY);
  if (pty->slave == -1)
    {
      std::cout << "open(SlaveName)" << std::endl;
      return false;
    }
  return true;
}

bool spawn(PTY* pty)
{
  pid_t p;
  char* env[] = { "TERM=xterm-256color", NULL };
  
  p = fork();
  if (p == 0)
    {
      close(pty->master);
      
      setsid();
      if (ioctl(pty->slave, TIOCSCTTY, NULL) == -1)
	{
	  std::cerr << "ioctl(TIOCSCTTY) failed" << std::endl;
	  return false;
	}
      
      dup2(pty->slave, 0);
      dup2(pty->slave, 1);
      dup2(pty->slave, 2);
      close(pty->slave);
      
      execle(SHELL, "-" SHELL, (char *)NULL, env);
      return false;
    }
  else if (p > 0)
    {
      close(pty->slave);
      return true;
    }
  
  std::cerr << "fork failed" << std::endl;
  return false;
}
