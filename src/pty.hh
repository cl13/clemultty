#ifndef __PTY__HH__
#define __PTY__HH__
#include <unistd.h>

#include "term.hh"

class PTY
{
public:
  PTY (); // ctor
  ~PTY(); // dtor
  int master, slave; // Master is the window, slave is the emulator
};

bool pt_pair(PTY*);
bool spawn(PTY*);

#endif
