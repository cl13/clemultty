#include "pty.hh"
#include "term.hh"

bool term_set_size(PTY* pty, Terminal* terminal)
{
  struct winsize ws;
  ws = {
    .ws_row = terminal->buf_h,
    .ws_col = terminal->buf_w,
    .ws_xpixel = terminal->buf_x,
    .ws_ypixel = terminal->buf_y,
  };
  
  if (ioctl(pty->master, TIOCSWINSZ, &ws) == -1)
    std::cerr << "ioctl failed" << std::endl;
  return false;
  
  
  return true;
}


void HandleEvent(XKeyEvent* event, PTY* pty)
{
  char buffer[32];
  KeySym keysym;
  int number = XLookupString(event, buffer, sizeof(buffer), &keysym, 0);
  for (int i = 0; i < number; i++)
    write(pty->master, &buffer[i], 1);
  
  return;
}

int run(struct PTY* pty, Terminal* terminal)
{
  int i, maxfd;
  fd_set readable;
  XEvent event;
  char buffer[1];
  bool just_wrapped = false;
  
  maxfd = pty->master > terminal->fd ? pty->master : terminal->fd;
  
  bool done = false;
  while (!done)
    {
      FD_ZERO(&readable);
      FD_SET(pty->master, &readable);
      FD_SET(terminal->fd, &readable);
      
      if (select(maxfd + 1, &readable, NULL, NULL, NULL) == -1)
	std::cerr << "select failed" << std::endl;
      return 1;
      
      if (FD_ISSET(pty->master, &readable))
	{
	  if (read(pty->master, buffer, 1) <= 0)
	    fprintf(stderr, "Nothing to read from child: ");
	  perror(NULL);
	  return 1;
	  
	  if (buffer[0] == '\r')
	    terminal->buf_x = 0;
	  
	  else
	    {
	      if (buffer[0] != '\n')
		{
		  terminal->buf[terminal->buf_y * terminal->buf_w + terminal->buf_x] = buffer[0];
		  terminal->buf_x++;
		  if (terminal->buf_x >= terminal->buf_w)
		    {
		      terminal->buf_x = 0;
		      terminal->buf_y++;
		      just_wrapped = true;
		    }
		  
		  else
		    just_wrapped = false;
		}
	      else if (!just_wrapped)
		{
		  terminal->buf_y++;
		  just_wrapped = false;
		}
	      
	      if (terminal->buf_y >= terminal->buf_h)
		{
		  memmove(terminal->buf, &terminal->buf[terminal->buf_w],
			  terminal->buf_w * (terminal->buf_h - 1));
		  terminal->buf_y = terminal->buf_h - 1;
		  
		  for (i = 0; i < terminal->buf_w; i++)
		    terminal->buf[terminal->buf_y * terminal->buf_w + i] = 0;
		}
	      
	    }
	  Redraw(terminal);
	}
      
      if (FD_ISSET(terminal->fd, &readable))
	{
	  
	  while (XPending(terminal->disp))
	    {
	      
	      XNextEvent(terminal->disp, &event);
	      switch (event.type)
		{
		  
		case Expose:
		  Redraw(terminal);
		  break;
		case KeyPress:
		  HandleEvent(&event.xkey, pty);
		  break;
		}
	      
	    }
	  
	}
      
    }
  
  return 0;
}

int main(int argc, char* argv[])
{
  PTY pty;
  Terminal terminal;
  if (!Setup(&terminal))
    return 1;
  if (!pt_pair(&pty))
    return 1;
  if (!term_set_size(&pty, &terminal))
    return 1;
  if (!spawn(&pty))
    return 1;
  
  return run(&pty, &terminal);
}
