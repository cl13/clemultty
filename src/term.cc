#include "term.hh"

void Redraw(Terminal* terminal)
{
  int x, y;
  char buffer[1];
  
  XSetForeground(terminal->disp, terminal->gc, terminal->col_bg);
  XFillRectangle(terminal->disp, terminal->termwin, terminal->gc, 0, 0, terminal->w, terminal->h);
  
  XSetForeground(terminal->disp, terminal->gc, terminal->col_fg);
  for (y = 0; y < terminal->buf_h; y++)
    {
      for (x = 0; x < terminal->buf_w; x++)
	{
	  buffer[0] = terminal->buf[y * terminal->buf_w + x];
	  if (!iscntrl(buffer[0]))
	    {
	      XDrawString(terminal->disp, terminal->termwin, terminal->gc,
			  x * terminal->font_width,
			  y * terminal->font_height + terminal->xfont->ascent,
			  buffer, 1);
	    }
	}
    }
  
  XSetForeground(terminal->disp, terminal->gc, terminal->col_fg);
  XFillRectangle(terminal->disp, terminal->termwin, terminal->gc,
		 terminal->buf_x * terminal->font_width,
		 terminal->buf_y * terminal->font_height,
		 terminal->font_width, terminal->font_height);
  
  XSync(terminal->disp, False);
  return;
}

bool Setup(Terminal* terminal)
{
  Colormap cmap;
  XColor color;
  XSetWindowAttributes wa =
    {
      .background_pixmap = ParentRelative,
      .event_mask = KeyPressMask | KeyReleaseMask | ExposureMask,
    };
  
  terminal->disp = XOpenDisplay(NULL);
  if (terminal->disp == NULL)
    {
      fprintf(stderr, "Cannot open display\n");
      return false;
    }
  
  terminal->screen = DefaultScreen(terminal->disp);
  terminal->root = RootWindow(terminal->disp, terminal->screen);
  terminal->fd = ConnectionNumber(terminal->disp);
  
  terminal->xfont = XLoadQueryFont(terminal->disp, "fixed");
  if (terminal->xfont == NULL)
    {
      fprintf(stderr, "Could not load font\n");
      return false;
    }
  terminal->font_width = XTextWidth(terminal->xfont, "m", 1);
  terminal->font_height = terminal->xfont->ascent + terminal->xfont->descent;
  
  cmap = DefaultColormap(terminal->disp, terminal->screen);
  
  if (!XAllocNamedColor(terminal->disp, cmap, "#000000", &color, &color))
    {
      fprintf(stderr, "Could not load bg color\n");
      return false;
    }
  terminal->col_bg = color.pixel;
  
  if (!XAllocNamedColor(terminal->disp, cmap, "#aaaaaa", &color, &color))
    {
      fprintf(stderr, "Could not load fg color\n");
      return false;
    }
  terminal->col_fg = color.pixel;
  terminal->buf_w = 80;
  terminal->buf_h = 25;
  terminal->buf_x = 0;
  terminal->buf_y = 0;
  terminal->buf = (char*) calloc(terminal->buf_w * terminal->buf_h, 1);
  if (terminal->buf == NULL)
    {
      std::cerr << "failed to calloc" << std::endl;
      return false;
    }
  
  terminal->w = terminal->buf_w * terminal->font_width;
  terminal->h = terminal->buf_h * terminal->font_height;
  terminal->termwin = XCreateWindow(terminal->disp, terminal->root,
				    0, 0,
				    terminal->w, terminal->h,
				    0,
				    DefaultDepth(terminal->disp, terminal->screen),
				    CopyFromParent,
				    DefaultVisual(terminal->disp, terminal->screen),
				    CWBackPixmap | CWEventMask,
				    &wa);
  XStoreName(terminal->disp, terminal->termwin, "CLX");
  XMapWindow(terminal->disp, terminal->termwin);
  terminal->gc = XCreateGC(terminal->disp, terminal->termwin, 0, NULL);
   
  XSync(terminal->disp, False);
  
  return true;
}
