CXX = g++
CXXFLAGS =
CPPFLAGS =
LINKERLIBS = -l X11
OBJS = src/pty.o src/term.o src/main.o
OUT = clemultty
PATH = /usr/bin/

build: $(OBJS)
	$(CXX) -o $(OUT) $(CPPFLAGS) $(CXXFLAGS) $(LINKERLIBS) $(OBJS)

clean:
	rm -rf $(OUT) $(OBJS)

install:
	sudo cp $(OUT) $(PATH)
